import React, { useEffect, useState } from 'react';


function ListAppointments() {
    const [appointmentsList, setAppointmentsList] = useState([]);
    async function loadAppointments() {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setAppointmentsList(data.appointments);
        };
    };
    useEffect(() => {
        loadAppointments();
    }, []);

    function prettyDateTime(isodatetime) {
        return (new Date(isodatetime).toLocaleString('en-US', {
            timeZone: 'UTC',
            year: 'numeric',
            month: 'numeric',
            day: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
        }));
    };

    async function CancelAppointment(id) {
        const cancelURL = `http://localhost:8080/api/appointments/${id}/cancel/`;
        const fetchConfig = {
            method: "put",
        };
        const appointmentCancel = await fetch(cancelURL, fetchConfig);
        if (appointmentCancel.ok) {
            window.location.reload();
        };
    };

    async function FinishAppointment(id) {
        const finishURL = `http://localhost:8080/api/appointments/${id}/finish/`;
        const fetchConfig = {
            method: "put",
        };
        const appointmentFinish = await fetch(finishURL, fetchConfig);
        if (appointmentFinish.ok) {
            window.location.reload();
        };
    };

    return (
        <>
            <h1>Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date & Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {appointmentsList?.map(appointment => {
                        return (
                            <tr key={ appointment.id } className={ appointment.status === "canceled" || appointment.status === "finished" ? "d-none": "" }>
                                <td>{ appointment.vin }</td>
                                <td>{ appointment.vip === true? "Yes": "No" }</td>
                                <td>{ appointment.customer }</td>
                                <td>{ prettyDateTime(appointment.date_time) }</td>
                                <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                                <td>{ appointment.reason }</td>
                                <td>
                                    <button onClick={() => CancelAppointment(appointment.id)}>Cancel</button>
                                </td>
                                <td>
                                    <button onClick={() => FinishAppointment(appointment.id)}>Finish</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
};

export default ListAppointments;
