import React, { useEffect, useState } from 'react';


function ListAutomobiles() {
    const [automobilesList, setAutomobilesList] = useState([]);
    async function loadAutomobiles() {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            setAutomobilesList(data.autos);
        };
    };
    useEffect(() => {
        loadAutomobiles();
    }, []);

    return (
        <>
            <h1>Automobiles</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {automobilesList?.map(automobile => {
                        return (
                            <tr key={ automobile.id }>
                                <td>{ automobile.vin }</td>
                                <td>{ automobile.color }</td>
                                <td>{ automobile.year }</td>
                                <td>{ automobile.model.name }</td>
                                <td>{ automobile.model.manufacturer.name }</td>
                                <td>{ automobile.sold === true? "Yes": "No" }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
};

export default ListAutomobiles;
