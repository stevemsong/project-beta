import React, {useEffect, useState} from 'react';


function CreateAutomobileForm() {
    const [models, setModels] = useState([]);
    const [alertState, setAlertState] = useState('alert alert-success d-none');
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    });

    const fetchData = async () => {
        const modelUrl = 'http://localhost:8100/api/models/';
        const response = await fetch(modelUrl);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        };
    };

    function sleep(ms) {
        return (new Promise(resolve => setTimeout(resolve, ms)));
    };

        useEffect(() => {
            fetchData();
        }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(automobileUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                color: '',
                year: '',
                vin: '',
                model_id: '',
            });
            setAlertState("mt-3 alert alert-success");
            await sleep(3000);
            setAlertState("mt-3 alert alert-success d-none");
        };
    };

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an Automobile to Inventory</h1>
                    <form onSubmit={handleSubmit} id="create-automobile-form">
                        <div className="form-floating mb-3">
                            <input value={formData.color} onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.year} onChange={handleFormChange} placeholder="Year" required type="number" name="year" id="year" className="form-control"/>
                            <label htmlFor="year">Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.vin} onChange={handleFormChange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control"/>
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="mb-3">
                            <select value={formData.model_id} onChange={handleFormChange} required name="model_id" id="model_id" className="form-select">
                            <option value="">Choose a model</option>
                            {models?.map(model => {
                                return(
                                    <option key={ model.id } value={ model.id }>{ model.name }</option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                    <div className={alertState} role="alert">
                        Automobile Added
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CreateAutomobileForm;
