import React, { useState } from 'react';


function CreateCustomerForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: '',
    });
    const [alertState, setAlertState] = useState('alert alert-success d-none');

    function sleep(ms) {
        return (new Promise(resolve => setTimeout(resolve, ms)));
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/customers/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application.json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                address: '',
                phone_number: '',
            });
            setAlertState("mt-3 alert alert-success");
            await sleep(3000);
            setAlertState("mt-3 alert alert-success d-none");
        };
    };

    const handleChange = event => {
        const { name, value } = event.target;
        setFormData(prevState => ({ ...prevState, [name]: value}));
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New Customer</h1>
                    <form onSubmit={handleSubmit} id="create-customer-form">
                        <div className="form-floating mb-3">
                        <input
                            onChange={handleChange}
                            value={formData.first_name}
                            placeholder="First Name"
                            required
                            type="text"
                            name="first_name"
                            id="first_name"
                            className="form-control"
                        />
                        <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input
                            onChange={handleChange}
                            value={formData.last_name}
                            placeholder="Last Name"
                            required
                            type="text"
                            name="last_name"
                            id="last_name"
                            className="form-control"
                        />
                        <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input
                            onChange={handleChange}
                            value={formData.address}
                            placeholder="Address"
                            required
                            type="text"
                            min="0"
                            name="address"
                            id="employeaddresse_id"
                            className="form-control"
                        />
                        <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input
                            onChange={handleChange}
                            value={formData.phone_number}
                            placeholder="Phone Number"
                            required
                            type="text"
                            min="0"
                            name="phone_number"
                            id="phone_number"
                            className="form-control"
                        />
                        <label htmlFor="phone_number">Phone Number</label>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                    <div className={alertState} role="alert">
                        Customer Added
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CreateCustomerForm;
