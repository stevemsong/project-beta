import React, { useEffect, useState } from 'react';


function ListVehicleModels() {
    const [vehicleModelsList, setVehicleModelsList] = useState([]);
    async function loadVehicleModels() {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
            const data = await response.json();
            setVehicleModelsList(data.models);
        };
    };

    useEffect(() => {
        loadVehicleModels();
    }, []);

    return (
        <>
            <h1>Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {vehicleModelsList?.map(model => {
                        return (
                            <tr key={ model.id }>
                                <td className="col-3">{ model.name }</td>
                                <td className="col-3">{ model.manufacturer.name }</td>
                                <td className="col-3"><img src={ model.picture_url } alt="vehicle model" width="300" height="200"/></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
};

export default ListVehicleModels;
