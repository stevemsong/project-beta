import React, { useState } from 'react';


function CreateSalespersonForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    });
    const [alertState, setAlertState] = useState('alert alert-success d-none');

    function sleep(ms) {
        return (new Promise(resolve => setTimeout(resolve, ms)));
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/salespersons/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application.json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: '',
            });
            setAlertState("mt-3 alert alert-success");
            await sleep(3000);
            setAlertState("mt-3 alert alert-success d-none");
        };
    };

    const handleChange = event => {
        const { name, value } = event.target;
        setFormData(prevState => ({ ...prevState, [name]: value}));
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New Salesperson</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floating mb-3">
                        <input
                            onChange={handleChange}
                            value={formData.first_name}
                            placeholder="First Name"
                            required
                            type="text"
                            name="first_name"
                            id="first_name"
                            className="form-control"
                        />
                        <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input
                            onChange={handleChange}
                            value={formData.last_name}
                            placeholder="Last Name"
                            required
                            type="text"
                            name="last_name"
                            id="last_name"
                            className="form-control"
                        />
                        <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input
                            onChange={handleChange}
                            value={formData.employee_id}
                            placeholder="Employee ID"
                            required
                            type="text"
                            min="0"
                            name="employee_id"
                            id="employee_id"
                            className="form-control"
                        />
                        <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                    <div className={alertState} role="alert">
                        Salesperson Added
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CreateSalespersonForm;
