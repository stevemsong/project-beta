import { Link } from 'react-router-dom';


function Nav() {
	return (
		<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
			<div className="container-fluid">
				<Link className="navbar-brand" to="/">NS Auto</Link>
				<button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span className="navbar-toggler-icon"></span>
				</button>
				<div className="collapse navbar-collapse" id="navbarSupportedContent">
					<ul className="navbar-nav me-auto mb-2 mb-lg-0">
						<li className="nav-item dropdown">
							<a className="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Sales</a>
							<ul className="dropdown-menu navbar-dark bg-dark" aria-labelledby="navbarDropdown">
								<li>
									<Link className="dropdown-item text-white" to="/salespersons">Salespersons</Link>
								</li>
								<li>
									<Link className="dropdown-item text-white" to="/salespersons/new">Add a Salesperson</Link>
								</li>
								<li>
									<Link className="dropdown-item text-white" to="/customers">Customers</Link>
								</li>
								<li>
									<Link className="dropdown-item text-white" to="/customers/new">Add a Customer</Link>
								</li>
								<li>
									<Link className="dropdown-item text-white" to="/sales">Sales</Link>
								</li>
								<li>
									<Link className="dropdown-item text-white" to="/sales/new">Record Sale</Link>
								</li>
								<li>
									<Link className="dropdown-item text-white" to="sales/history">Sales History</Link>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<a className="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Services</a>
							<ul className="dropdown-menu navbar-dark bg-dark" aria-labelledby="navbarDropdown">
								<li>
									<Link className="dropdown-item text-white" to="/technicians">Technicians</Link>
								</li>
								<li>
									<Link className="dropdown-item text-white" to="/technicians/new">Add a Technician</Link>
								</li>
								<li>
									<Link className="dropdown-item text-white" to="/appointments">Service Appointments</Link>
								</li>
								<li>
									<Link className="dropdown-item text-white" to="/appointments/new">Create a Service Appointment</Link>
								</li>
								<li>
									<Link className="dropdown-item text-white" to="/appointments/history">Service History</Link>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<a className="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Inventory</a>
							<ul className="dropdown-menu navbar-dark bg-dark" aria-labelledby="navbarDropdown">
								<li>
									<Link className="dropdown-item text-white" to="/manufacturers">Manufacturers</Link>
								</li>
								<li>
									<Link className="dropdown-item text-white" to="/manufacturers/new">Add a Manufacturer</Link>
								</li>
								<li>
									<Link className="dropdown-item text-white" to="/models">Models</Link>
								</li>
								<li>
									<Link className="dropdown-item text-white" to="/models/new">Add a Model</Link>
								</li>
								<li>
									<Link className="dropdown-item text-white" to="/automobiles">Automobiles</Link>
								</li>
								<li>
									<Link className="dropdown-item text-white" to="/automobiles/new">Add an Automobile</Link>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	);
};

export default Nav;
