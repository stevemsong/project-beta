import React, { useEffect, useState } from 'react';


function ListManufacturers() {
    const [manufacturersList, setManufacturersList] = useState([]);
    async function loadManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const data = await response.json();
            setManufacturersList(data.manufacturers);
        };
    };

    useEffect(() => {
        loadManufacturers();
    }, []);

    return (
        <>
            <h1>Manufacturers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturersList?.map(manufacturer => {
                        return (
                            <tr key={ manufacturer.id }>
                                <td>{ manufacturer.name }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
};

export default ListManufacturers;
