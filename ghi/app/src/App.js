import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListSalespersons from './ListSalespersons';
import CreateSalespersonForm from './CreateSalespersonForm';
import ListCustomers from './ListCustomers';
import CreateCustomerForm from './CreateCustomerForm';
import ListSales from './ListSales';
import CreateSaleForm from './CreateSaleForm';
import ListSalespersonHistory from './ListSalespersonHistory';
import ListTechnician from './ListTechnicians';
import CreateTechnicianForm from './CreateTechnicianForm';
import ListAppointments from './ListAppointments';
import CreateAppointmentForm from './CreateAppointmentForm';
import ListServiceHistory from './ListServiceHistory';
import ListManufacturers from './ListManufacturers';
import CreateManufacturerForm from './CreateManufacturerForm';
import ListVehicleModels from './ListVehicleModels';
import CreateVehicleModelForm from './CreateVehicleModelForm';
import ListAutomobiles from './ListAutomobiles';
import CreateAutomobileForm from './CreateAutomobileForm';


function App(props) {
	return (
		<BrowserRouter>
		<Nav />
		<div className="container">
			<Routes>
				<Route path="/" element={<MainPage />} />
				<Route path="salespersons" element={<ListSalespersons />} />
				<Route path="salespersons/new" element={<CreateSalespersonForm />} />
				<Route path="customers" element={<ListCustomers />} />
				<Route path="customers/new" element={<CreateCustomerForm />} />
				<Route path="sales" element={<ListSales />} />
				<Route path="sales/new" element={<CreateSaleForm />} />
				<Route path="sales/history" element={<ListSalespersonHistory />} />
				<Route path="technicians" element={<ListTechnician />}/>
				<Route path="technicians/new" element={<CreateTechnicianForm />}/>
				<Route path="appointments" element={<ListAppointments />}/>
				<Route path="appointments/new" element={<CreateAppointmentForm />}/>
				<Route path="appointments/history" element={<ListServiceHistory />}/>
				<Route path="manufacturers" element={<ListManufacturers />}/>
				<Route path="manufacturers/new" element={<CreateManufacturerForm />}/>
				<Route path="models" element={<ListVehicleModels />}/>
				<Route path="models/new" element={<CreateVehicleModelForm />}/>
				<Route path="automobiles" element={<ListAutomobiles />}/>
				<Route path="automobiles/new" element={<CreateAutomobileForm />}/>
			</Routes>
		</div>
		</BrowserRouter>
	);
};

export default App;
