from django.db import models


class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=200)

    def __str__(self):
        return self.employee_id


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.TextField()
    phone_number = models.CharField(max_length=13)

    def __str__(self):
        return self.last_name


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=200)

    def __str__(self):
        return self.vin


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name='automobile',
        on_delete=models.PROTECT,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name='salesperson',
        on_delete=models.PROTECT,
    )
    customer = models.ForeignKey(
        Customer,
        related_name='customer',
        on_delete=models.PROTECT,
    )
    price = models.PositiveBigIntegerField()

    def __str__(self):
        return (
            f"{self.salesperson.first_name} {self.salesperson.last_name} "
            f"sold {self.automobile.vin} to "
            f"{self.customer.first_name} {self.customer.last_name} "
            f"for ${self.price}"
        )
