import React, { useEffect, useState } from 'react';


function ListSales() {
    const [salesList, setSalesList] = useState(null);
    async function loadSales() {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            setSalesList(data.sales);
        };
    };

    useEffect(() => {
        loadSales();
    }, []);

    return (
        <>
            <h1>Sales</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>Automobile VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {salesList?.map(sale => {
                        return (
                            <tr key={ sale.id }>
                                <td>{ sale.salesperson.employee_id }</td>
                                <td>{ sale.salesperson.first_name} {sale.salesperson.last_name }</td>
                                <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                                <td>{ sale.automobile.vin }</td>
                                <td>${ sale.price }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
};

export default ListSales;
