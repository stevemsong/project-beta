# NS Auto

Team:

* Nick - Sales
* Steve - Services

## Design

The website has three bounded contexts: Sales, Service, and Inventory. Each functions as a separate microservice. Sales and service access the inventory through the Automobile VO.
Sales handles the selling of unsold inventory vehicles, and each sale records the automobile sold, salesperson, customer and price of the sold automobile.
Service handles service appointments for automobiles, and each appointment includes the automobile VIN, customer, date/time, technician, and reason. If the automobile VIN matches the inventory, the customer is given VIP status.
Inventory handles automobiles to be sold, while also retaining sold vehicle info for conferring VIP status for service, among other uses. Each automobile has a manufacturer, model name, color, year, and VIN.


## Service microservice

I created a Technician model, Appointment model, and AutomobileVO model. The Automobile value objects are being polled from the inventory microservice. When an appointment is created it checks if the vin input matches any vin from the list of Automobile value objects we are polling. If there is a match, the vip attribute will be marked as true.

## Sales microservice

For Sales, I created a Salesperson model, a Customer model, an AutomobileVO model, and a Sale model. Each sale has a salesperson, customer, and automobile (via the AutomobileVO) attached to it. The AutomobileVO acts in tandem with the poller to communicate data both to and from the inventory: in the event of a sale, the vehicle in the inventory microservice is marked as "sold"; when a sale is being recorded, it is displaying only vehicles that are marked as unsold to be sold.
